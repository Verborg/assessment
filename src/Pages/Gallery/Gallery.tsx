import React, { useState, useEffect, Dispatch, SetStateAction } from "react";
import axios from "axios";
import "./Gallery.scss";
import { Breeds, PicturesQuery } from "../../Interfaces";
import { Link } from "react-router-dom";
import SideMenu from "./Components/SideMenu/SideMenu";
import PictureGrid from "./Components/PictureGrid/PictureGrid";

interface QueryObj {
  url: string;
  setState:
    | Dispatch<SetStateAction<Breeds>>
    | Dispatch<SetStateAction<string[]>>;
}

export default function Gallery() {
  const [breeds, setBreeds] = useState<Breeds>({});
  const [picturesQuery, setPicturesQuery] = useState<PicturesQuery>({
    breed: "",
  });
  const [breedPictures, setBreedPictures] = useState<string[]>([]);

  useEffect(() => {
    fetchData({
      url: "https://dog.ceo/api/breeds/list/all",
      setState: setBreeds,
    });
  }, []);

  useEffect(() => {
    const createFetchObj = () => {
      const { breed, subBreed } = picturesQuery;
      const breedUrl = `https://dog.ceo/api/breed/${breed}/images`;
      const subBreedUrl = `https://dog.ceo/api/breed/${breed}/${subBreed}/images`;
      return {
        url: subBreed ? subBreedUrl : breedUrl,
        setState: setBreedPictures,
      };
    };

    if (!picturesQuery.breed) return;
    const queryObj = createFetchObj();
    fetchData(queryObj);
  }, [picturesQuery]);

  const fetchData = (queryObj: QueryObj) => {
    const { url, setState } = queryObj;
    axios
      .get(url)
      .then((res) => {
        setState(res.data.message);
        window.scrollTo(0, 0);
      })
      .catch(() => {
        window.alert("Unfortunately something went wrong with the request");
      });
  };

  return (
    <div className="gallery-root">
      <h1 data-testid="galleryHeader">Welcome to the dog picture gallery</h1>
      <div className="gallery-content">
        <SideMenu setPicturesQuery={setPicturesQuery} breeds={breeds} />
        {breedPictures.length > 0 ? (
          <PictureGrid breedPictures={breedPictures} />
        ) : (
          <h2>
            Click any of the (sub)breeds on the left to see what this dog looks
            like
          </h2>
        )}
      </div>
    </div>
  );
}
