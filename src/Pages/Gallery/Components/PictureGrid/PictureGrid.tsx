import React from "react";
import "./PictureGrid.scss";

interface Props {
  breedPictures: string[];
}

function PictureGrid({ breedPictures }: Props) {
  return (
    <div className="picture-grid-root">
      {breedPictures.map((url) => (
        <img loading="lazy" src={url} />
      ))}
    </div>
  );
}

export default PictureGrid;
