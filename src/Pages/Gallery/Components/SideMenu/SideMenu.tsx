import React, { Dispatch, SetStateAction } from "react";
import { Breeds, PicturesQuery } from "../../../../Interfaces";
import "./SideMenu.scss";

interface Props {
  breeds: Breeds;
  setPicturesQuery: Dispatch<SetStateAction<PicturesQuery>>;
}

function SideMenu({ breeds, setPicturesQuery }: Props) {
  return (
    <div className="sidemenu-root">
      {Object.keys(breeds).map((breed) => (
        <div className="menu-item">
          <h3
            className="menu-breed-header"
            onClick={() => setPicturesQuery({ breed: breed })}
          >
            {breed}
          </h3>
          <div>
            {breeds[breed].map((subBreed) => (
              <p
                className="menu-subbreed-header"
                onClick={() =>
                  setPicturesQuery({ breed: breed, subBreed: subBreed })
                }
              >
                {subBreed}
              </p>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}

export default SideMenu;
