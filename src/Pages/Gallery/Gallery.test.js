import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import Gallery from "./Gallery";

test("is gallery header present in the document", () => {
  const { getByTestId } = render(<Gallery />);
  const galleryHeader = getByTestId("galleryHeader");
  expect(galleryHeader).toBeInTheDocument();
});
