import React from "react";
import "./Welcome.scss";

function Welcome() {
  return (
    <div className="welcome-root">
      <div className="welcome-window">
        Hey guys, thank you for taking the time to review my code. I ended up
        choosing the Dog api and wrote some code based on what the api offered
        me. While writing my code I primarily focused on the fetching of data
        and sharing it between components. I tried to make the fetching as DRY
        and dynamic as possible and I'm looking forward to discussing the
        approach I took. Because of this focus on data fetching, styling-wise
        the assessment turned out to be the most 90's looking website I've seen
        in a while :P Finally, for the unit test I basically wrote the 'hello
        world' of unit tests. I have very little experience with testing and
        this is something I would love to learn. Unfortunately, at my current
        employer there is no time or money allocated towards testing.
      </div>
    </div>
  );
}

export default Welcome;
