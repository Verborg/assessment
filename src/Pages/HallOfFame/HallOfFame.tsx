import React from "react";
import { Link } from "react-router-dom";

function HallOfFame() {
  return (
    <div>
      <h1>Hall Of Fame</h1>
      <p>
        Honerable mentions go out to the creator of the Dog API and of course
        our own dog Apollo!
      </p>
      <img src="/apollo1.jpeg" />
    </div>
  );
}

export default HallOfFame;
