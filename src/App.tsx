import "./App.scss";
import Gallery from "./Pages/Gallery/Gallery";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import HallOfFame from "./Pages/HallOfFame/HallOfFame";
import Welcome from "./Pages/Welcome/Welcome";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <nav className="main-nav">
          <Link to="/">
            <p>Home</p>
          </Link>
          <Link to="/gallery">
            <p>Gallery</p>
          </Link>
          <Link to="/hall-of-fame">
            <p>Hall Of Fame</p>
          </Link>
        </nav>
        <Routes>
          <Route path="/" element={<Welcome />} />
          <Route path="hall-of-fame" element={<HallOfFame />} />
          <Route path="gallery" element={<Gallery />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
