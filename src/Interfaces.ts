export interface Breeds {
  [key: string]: string[];
}

export interface PicturesQuery {
  breed: string;
  subBreed?: string;
}
